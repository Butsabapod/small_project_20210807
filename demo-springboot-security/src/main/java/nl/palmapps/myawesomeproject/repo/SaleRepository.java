package nl.palmapps.myawesomeproject.repo;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import nl.palmapps.myawesomeproject.model.Sale;

@Repository
public interface SaleRepository extends CrudRepository<Sale, Integer> {

}