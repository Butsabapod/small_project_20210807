package nl.palmapps.myawesomeproject.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import nl.palmapps.myawesomeproject.model.UserTestTest;

@Repository
public interface UserTestTestRepository extends 
CrudRepository<UserTestTest, Integer>{

}
