package nl.palmapps.myawesomeproject.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Sale implements Serializable {
	 private static final long serialVersionUID = 6832006422622219737L;
	 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String name;
	private int salary;
	
	@ManyToOne 
	UserTestTest user;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public UserTestTest getUser() {
		return user;
	}
	public void setUser(UserTestTest user) {
		this.user = user;
	}
		
	
}
