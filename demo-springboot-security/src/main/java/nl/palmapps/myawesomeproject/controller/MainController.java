package nl.palmapps.myawesomeproject.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import nl.palmapps.myawesomeproject.exception.InputFieldException;
import nl.palmapps.myawesomeproject.model.Greeting;
import nl.palmapps.myawesomeproject.model.Sale;
import nl.palmapps.myawesomeproject.model.UserTestTest;
import nl.palmapps.myawesomeproject.repo.SaleRepository;
import nl.palmapps.myawesomeproject.repo.UserTestTestRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Example controller to test security calls
 */
@RestController
@RequestMapping("/api")
public class MainController {

    private static final String TEMPLATE = "Hello, %s!";
    private static final String TEMPLATE_ADMIN = "Hello Admin, %s!";
    private final AtomicLong counter = new AtomicLong();
    private static final Logger logger = LoggerFactory.getLogger(InputFieldException.class);

 	@Autowired UserTestTestRepository userRepo;
 	@Autowired SaleRepository saleRepo;
 	
 	ArrayList<UserTestTest> userList = new ArrayList<UserTestTest>();

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/hello/admin")
    public Greeting greetingAdmin(@RequestParam(value = "name", defaultValue = "World") String name) {

        return new Greeting(counter.incrementAndGet(),
                String.format(TEMPLATE_ADMIN, name));
    }

    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("/hello/user")
    public Greeting greetingUser(@RequestParam(value = "name", defaultValue = "World") String name) {

        return new Greeting(counter.incrementAndGet(),
            String.format(TEMPLATE, name));
    }

    @PostMapping("/")
    public Greeting homePage(@RequestParam(value = "name", defaultValue = "World") String name) {

        return new Greeting(counter.incrementAndGet(),
                String.format(TEMPLATE, name));
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping({"/user", "/me"})
    public ResponseEntity<?> user(Principal principal) {
        return ResponseEntity.ok(principal);
    }
    
 
	

	@PreAuthorize("isAuthenticated()")
	@GetMapping("/test")
	@ResponseBody
	String test() {
		System.out.println(1/0);
		return "";
	}
	
	@PreAuthorize("isAuthenticated()")
	@GetMapping("/users")
	@ResponseBody
	String getAllUsers() {
		Iterable<UserTestTest> users =  userRepo.findAll();
		String out = "";
		for (UserTestTest user : users) {
			out = out + user.getUsername();
		}
		return "Get User " + out;
	}
	
	
    @PreAuthorize("isAuthenticated()")
	@ResponseStatus(value=HttpStatus.CONFLICT, reason="Input field is wrong")  
	@ExceptionHandler(InputFieldException.class)
	public void handleInputFieldException() {
	    System.out.println("Handle Input Field Exception");
	    logger.debug("This is Debug");
	    logger.info("This is Info");
	    logger.warn("This is warning");
	    logger.error("This is error ");
	    
	}

    @PreAuthorize("isAuthenticated()")
	@PostMapping(path = "/users", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String addUser(@RequestBody UserTestTest user) throws InputFieldException {
		if(user.getAge() > 100 || user.getAge() < 0) {
			throw new InputFieldException(false,false, true,false);
		}
		
		
		userRepo.save(user);
		
		return "Add User " + user.getUsername() + " password " + user.getPassword();
	}
	// Exception ที่อาจจะเกิดขึ้น 
	// 1. userId ที่ส่งมา ไม่มีใน Database
	// 2. ข้อมูลที่ส่งมา ไม่เป็น JSON VAlue หรือ JSON ผิด format 
	// 3. สิ่งที่ส่งมา ไม่ตรง Format ของ USerTestTest หรือส่ง Field มาไม่ครบ
	// 4. 
	// 5. 
	// 6. 
    @PreAuthorize("isAuthenticated()")
	@PutMapping(path = "/users/{userId}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String updateUser(@PathVariable("userId") int userId, @RequestBody UserTestTest user) {
		// 1. Get User By ID 
		UserTestTest userFromDB = userRepo.findOne(userId);
		// 2. Assign New Value 
		userFromDB.setAge(user.getAge());
		userFromDB.setPassword(user.getPassword());
		userFromDB.setUserId(user.getUserId());
		userFromDB.setUsername(user.getUsername());
		// 3. Update the new value 
		userRepo.save(userFromDB);
		
		return "Update User " + userId;
	}

    @PreAuthorize("isAuthenticated()")
	@DeleteMapping("/users/{userId}")
	@ResponseBody
	String deleteUser(@PathVariable("userId") int userId) {
		UserTestTest userFromDB = userRepo.findOne(userId);
		userRepo.delete(userFromDB);
		return "Delete User " + userId;
	}
    
    @PreAuthorize("isAuthenticated()")
    @PostMapping(path = "/users/sale", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String addUserSale(@RequestBody UserTestTest user) throws InputFieldException {
		if(user.getAge() > 100 || user.getAge() < 0) {
			throw new InputFieldException(false,false, true,false);
		}
		
		Set<Sale>sales = new  HashSet<Sale>();
		Sale sales1 = new Sale();
		sales1.setName("But1");
		sales1.setSalary(5000);
		sales.add(sales1);
		
		Sale sales2 = new Sale();
		sales2.setName("But2");
		sales2.setSalary(4000);
		sales.add(sales2);
		
		userRepo.save(user);
		sales1.setUser(user);
		sales2.setUser(user);
		saleRepo.save(sales1);
		saleRepo.save(sales2);
		user.setSales(sales);
		userRepo.save(user);
		
		return "Add User " + user.getUsername() + " password " + user.getPassword() +"Add sale Complete";
	}
    
}